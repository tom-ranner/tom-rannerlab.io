---
# Display name
title: Thomas Ranner

# Is this the primary user of the site?
superuser: true

# Role/position
role: "Associate Professor"

# Organizations/Affiliations
organizations:
- name: School of Computer Science
  url: "https://eps.leeds.ac.uk/computing"
- name: University of Leeds
  url: "https://www.leeds.ac.uk"

# Short bio (displayed in user profile at end of posts)
bio: ""

# List (academic) interests or hobbies
interests:
- Numerical analysis
- Nonlinear partial differential equations
- Free boundary problems
- Computational modelling
- Biological applications including microswimmers

# List qualifications (such as academic degrees)
education:
  courses:
  - course: PhD in Mathematics
    institution: University of Warwick
    year: 2013
  - course: MMath in Mathematics
    institution: University of Warwick
    year: 2009

# Social/Academic Networking
social:
- icon: envelope
  icon_pack: "fas"
  link: "#contact"
- icon: twitter
  icon_pack: "fab"
  link: "https://twitter.com/tomranner"
- icon: google-scholar
  icon_pack: ai
  link: "https://scholar.google.co.uk/citations?user=dS8Hj_AAAAA"
- icon: "arxiv"
  icon_pack: "ai"
  link: "https://arxiv.org/a/ranner_t_1.html"
- icon: "orcid"
  icon_pack: "ai"
  link: "http://orcid.org/0000-0001-7682-3175"
- icon: mastodon
  icon_pack: "fab"
  link: "https://mathstodon.xyz/@tr"
  rel: "me"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon: "cv"
#   icon_pack: "ai"
#   link: "files/cv.pdf"

---

I am an Associate Professor in the [School of Computer Science](https://eps.leeds.ac.uk/computing) at the University of Leeds.
My research focuses on the mathematical understanding of computational algorithms.
This work includes modelling, analysis and simulation using skills from maths, computer science, physics and biology.
I apply these ideas to the study of moving interface and free boundary problems in particular concerning the undulatory locomotion of microswimmers as part of the [Leeds wormlab](http://leeds.wormlab.eu).

## Funded PhD  project

I have funding for a PhD project on [Numerical computation of interface problems](https://phd.leeds.ac.uk/project/1156-numerical-computation-of-interface-problems). Please [contact me](#contact) for more details.

<div style="display:none">
<a rel="me" href="https://mathstodon.xyz/@tr">Mastodon</a>
</div>
