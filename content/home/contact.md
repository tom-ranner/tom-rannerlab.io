---
# An instance of the Contact widget.
# Documentation: https://sourcethemes.com/academic/docs/page-builder/
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

title: Contact
subtitle:

content:
  # Automatically link email and phone or display as text?
  autolink: true

  # Email form provider
  form:
    provider: ""

# Contact details (edit or remove options as required)
  email: T.Ranner@leeds.ac.uk
  phone: +44 (0) 113 343 4699
  address:
    street: Room 3.40d, Sir William Henry Bragg Building, University of Leeds
    cite: Leeds
    postcode: LS2 9JT
    country: UK
  contact_links:
    - icon: map
      icon_pack: fas
      name: University of Leeds campus map
      link: 'https://www.leeds.ac.uk/campusmap?location=17545'

design:
  columns: '2'
---
