---
title: "Origami Worms"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-05-21T15:30:53+01:00
lastmod: 2020-05-21T15:30:53+01:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
aliases:
  - /origami-worms
---
In the School of Computing at the University of Leeds we want to find out more about how worms move by wiggling. Can you help us by making your own origami worm?

# Materials

You will need:

- 14cm x 3cm paper or card
- straw
- scissors
- things to decorate your worm

![](/img/origami-worms/2020-05-21-135119.jpg)

# Instructions

  <div style="position:relative;padding-top: 56.25%">
<iframe style="position:absolute;top:0;left:0;width:100%;height:100%" src="https://www.youtube-nocookie.com/embed/wrlP9CK-u3s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>

## Step by step

1. Take the card or paper and fold in half length ways

	![](/img/origami-worms/2020-05-21-135151.jpg)
	![](/img/origami-worms/2020-05-21-135201.jpg)

2. Take each side and fold inwards

	![](/img/origami-worms/2020-05-21-135217.jpg)
	![](/img/origami-worms/2020-05-21-135230.jpg)

3. ... and in half inwards again

	![](/img/origami-worms/2020-05-21-135243.jpg)
	![](/img/origami-worms/2020-05-21-135254.jpg)

4. Unfold and it should now look like this

	![](/img/origami-worms/2020-05-21-135326.jpg)

5. Fold back up and carefully cut the two short edges to make a nice curve

	![](/img/origami-worms/2020-05-21-135348.jpg)
	![](/img/origami-worms/2020-05-21-135404.jpg)

6. Decorate your worm and give it a face so it knows which way to go

	![](/img/origami-worms/2020-05-21-135442.jpg)

7. Blow air with a straw

	![](/img/origami-worms/2020-05-21-135457.jpg)

# Variations

1. What happens if you make your worm out of card instead of paper? Why?
2. What happens if you make your worm bigger, wider or longer? Why?
3. What is the best surface to put your worm on to blow along? Why?
4. What happens if you don't cut the edge into a nice curve?
