+++
title = "EPSRC Funded Phd Studentship"
date = 2019-02-15T14:39:32Z
draft = false

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["funding", "PhD Student"]
categories = []

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
# Use `caption` to display an image caption.
#   Markdown linking is allowed, e.g. `caption = "[Image credit](http://example.org)"`.
# Set `preview` to `false` to disable the thumbnail in listings.
# [header]
# image = ""
# caption = ""
# preview = true

+++

*Edit:* The deadline for this funding has now passed. But please make contact if you are interested in pursuing one of these projects as alternative funding may be available.

Background:
===========

PhD students are sought in the School of Computing at the University of Leeds, UK. These studentships form part of a larger collaborative and interdisciplinary project, that currently includes two academics (Thomas Ranner and Netta Cohen), a postdoctoral fellow and three PhD students.

We study the neuromechanical basis of behaviour in the nematode worm C. elegans. Research combines biological experiments, mathematical and computational modelling of the neural control as well as investigations of the physics of the worm and its interaction with the environment. We are seeking to appoint up to three further PhD candidates: one on the mathematical understanding of numerical methods for biomechanical models (with Thomas Ranner), one on modelling the neural control of behaviour (with Netta Cohen) and one experimental project, combining behavioural experiments and machine vision (with Netta Cohen).  All projects are envisioned within this multidisciplinary setting. You will join a multi-disciplinary, dynamic, and creative group within the School of Computing at the University of Leeds, with close ties to the Fluid Dynamics Centre for Doctoral Training and to the Hope Laboratory in the Faculty of Biological Sciences, where additional biological experimental facilities are housed.

Informal enquires are welcome from all potential candidates. For more details please contact Dr Thomas Ranner (T.Ranner@leeds.ac.uk) and/or Prof Netta Cohen (N.Cohen@leeds.ac.uk).

Funding:
========

- *Funded PhD project:* UK and EU
- *Value:* A full standard studentship consists of academic fees (£4,327 in Session 2019/20), together with a maintenance grant (£15,009 in Session 2019/20) paid at standard Research Council rates. UK applicants will be eligible for a full award paying tuition fees and maintenance. European Union applicants will be eligible for an award paying tuition fees only, except in exceptional circumstances, or where residency has been established for more than 3 years prior to the start of the course.
- *Number of awards:* Various
- *Deadline:* 15/03/2019
- *Link:* https://engineering.leeds.ac.uk/research-opportunity/201323/research-degrees/2824/advanced-computing-(epsrc-dtp)

Although the funding is only available for EU/UK applicants, we welcome interest for excellent international candidates also.

Project descriptions:
=====================

Project 1: Modelling, simulation, analysis of Cosserat rods with application to microswimmers
---------------------------------------------------------------------------------------------
- Link: https://engineering.leeds.ac.uk/research-opportunity/201323/research-degrees/2844/modelling,-simulation,-analysis-of-cosserat-rods-with-application-to-microswimmers-(epsrc-dtp)
- Suggested start date October 2019.

The theory of Cosserat rods is very classical and understood from many different view points. A Cosserat rod is a long, thin structure which can bend, twist, stretch and shear. Cosserat rods have recently been proposed as the fundamental building block for modelling long, thin microswimmers (such as sperm cells or snakes). In this project, we are interested in developing finite element methods for their dynamical simulation. We aim to produce efficient, accurate and robust computation tools. The demonstration of the good properties comes by both detailed numerical analysis as well as thorough simulation.

The specific aspects  of this project to be worked on will depend on the strengths of the candidate. Different contributions to this project are welcome.

*Entry Requirements:* Candidates should have a strong undergraduate degree in a relevant area (including but not restricted to mathematics, physics or computer science). A good candidate for this project should have some knowledge of aspects of the design, analysis and implementation of finite element methods for partial differential equations. Programming (implementing novel finite element methods) is an essential part of this project. Prior experience in research (e.g. a Masters degree or work in industry) is a plus.



Project 2: Integrated neuro-mechanical modelling of C. elegans
--------------------------------------------------------------

- Link: https://engineering.leeds.ac.uk/research-opportunity/201323/research-degrees/2836/integrate-neuro-mechanical-modelling-of-c-elegans-(epsrc-dtp)
- Suggested start date October 2019.

The microscopic roundworm Caenorhabditis elegans is a relatively simple animal, with a small and fully mapped anatomy and nervous system. Dubbed the “hydrogen atom” of systems neuroscience, it is also the subject of intensifying efforts to model this creature completely. We are interested in understanding the neuromechanical control of locomotion in this animal. We are particularly interested in integrating our understanding of motor control and coordination between the head and along the body of the animal. This requires the construction and testing of computational models of the dynamics of neural circuits that are integrated within a biomechanical framework. We are interested in developing and implementing the models, validating them against experimental observations, performing dynamical systems analysis, e.g. to identify targets and modes of modulation, and generating experimentally testable predictions.

Specific projects will be chosen from the scope and remit of the project, but tailored to individual interests and skills.

*Entry Requirements:* All candidates should have a strong undergraduate degree in a relevant area (including but not restricted to mathematics, physics or computer science). A good candidate for this project should have some knowledge of nonlinear differential equations and an interest in dynamical systems. Interest and knowledge in biomechanics and/or computational neuroscience are a plus, but not required.  Prior experience in research (e.g. a Masters degree, or work in industry) is a plus. Programming and extensive simulations are an essential part of this project.

Project 3: Understanding C. elegans locomotion in 3D
----------------------------------------------------

- Link: https://engineering.leeds.ac.uk/research-opportunity/201323/research-degrees/2845/understanding-c-elegans-locomotion-in-3d-(epsrc-dtp)
- Suggested start date October 2019.

The microscopic roundworm Caenorhabditis elegans is long and thin, and moves by undulating its body. While its locomotion behaviours have been characterized in detail on the surface of a dish, little is known about its locomotion gait or the neural control of locomotion in more natural habitats. In the Cohen lab, we have been recording the motion of the animal in 3D volumes. In this project, we are interested in comparing behaviours, postures and muscle activation patterns in different media, and across different genetic strains.

Specific projects will be chosen from the scope and remit of the project, but tailored to individual interests and skills. All projects are likely to include some combination of:

- Biological experiments – behavioural assays, bright field and/or fluorescent imaging, optogenetics and molecular genetics.

- Algorithms & software development in machine vision and engineering for imaging, real time tracking and image/movie analysis (in particular 3D spatiotemporal reconstructions).

*Entry Requirements:* All candidates should have a strong undergraduate degree in a relevant area (including but not restricted to physics, biology, computer science or engineering). A good candidate for this project should be willing to learn experimental techniques and to combine experiments with data analysis and computer vision.  Prior experience in research (e.g. a Masters degree, or work in industry) is a plus. Programming and extensive simulations are an essential part of this project, and fluency in at least one programming language is a plus.


Additional information for all projects:
========================================

*About us:* The School of Computing has a strong track record in both research and teaching (top 10 in the UK in the 2019 Guardian league table). We have an excellent track record of high quality research in the foundations of computer science including including algorithms and complexity, numerical algorithms and analysis, high performance and distributed computing, as well as in a variety of applied and multidisciplinary settings. Of particular relevance here is our expertise in the areas of artificial intelligence, computer vision biororbotics and computational biomedicine (including computational neuroscience, biomechanics and biophysics as well as computational medicine). 

We are committed to equality and diversity and welcome applications from women and minorities. The Faculty of Engineering (including the School of Computing) has received a prestigious Athena SWAN Silver Award from the Equality Challenge Unit, the national body that promotes gender equality in the higher education sector. The Silver Award demonstrates that the Faculty has taken positive actions to ensure that policies, processes and ethos all promote an equal and inclusive environment for work and study.

*Home to apply:* Formal applications for research degree study should be made online through the university's website. Please state clearly in the research information section which project you wish to be considered for as well as your proposed supervisor.

If English is not your first language, you must provide evidence that you meet the University’s minimum English Language requirements.

We welcome scholarship applications from all suitably-qualified candidates, but UK black and minority ethnic (BME) researchers are currently under-represented in our Postgraduate Research community, and we would therefore particularly encourage applications from UK BME candidates. All scholarships will be awarded on the basis of merit.

If you require any further information please contact the Graduate School Office
e: phd@engineering.leeds.ac.uk, t: +44 (0)113 343 8000.

Informal enquires are welcome from all potential candidates. For more details please contact Dr Thomas Ranner (T.Ranner@leeds.ac.uk) and/or Prof Netta Cohen (N.Cohen@leeds.ac.uk). 
