---
title: "Interfaces and free boundaries 2021"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2021-06-15T16:45:00+01:00
lastmod: 2021-06-15T16:45:00+01:00
featured: false
draft: false
aliases:
  - /interfaces2021

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

I will be giving a talk at the Preliminary meeting for Interfaces and Free Boundaries.

# A stable finite element method for low Reynolds undulatory locomotion

## Abstract

We present and analyse a numerical method for understanding the dynamics of an open, inextensible viscoelastic rod - a long and thin three dimensional object. Our model allows for both elastic and viscous, bending and twisting deformations and describes the evolution of the midline curve of the rod as well as an orthonormal frame which full determines the rod's three dimensional geometry.
The numerical method is based on using a combination of piecewise linear and piecewise constant functions based on a novel rewriting of the model equations. We derive a stability estimate for the semi-discrete scheme and show that at the fully discrete level that we have good control over the length element and preserve the frame orthonormality conditions up to machine precision.
Numerical experiments demonstrate both the good properties of the method as well as the applicability of the method for simulating locomotion of the microscopic nematode Caenorhabditis elegans.

I will also talk about some recent work incorporating data form a novel 3D experimental set-up to infer control parameters and if time allows an implementation of the method into an outreach game.

## Slides

<div style="position:relative;padding-top:66%;">
  <iframe src="https://tom-ranner.gitlab.io/interfaces-and-free-boundaries-2021/index.embed.html" frameborder="0" allowfullscreen
    style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
</div>

<div class="btn-links mb-3">
<a class="btn btn-outline-primary my-1 mr-1" href="/publication/2020_viscoelasticrod/" target="_blank" rel="noopener">
  Paper
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://doi.org/10.1016/j.apnum.2020.05.009" target="_blank" rel="noopener">
  doi
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://tom-ranner.gitlab.io/interfaces-and-free-boundaries-2021/" target="_blank" rel="noopener">
  Slides
</a>
</div>

## Funding

This work was supported by a Leverhulme Trust early career fellowship.

