---
title: "Enumath2019"
subtitle: ""
summary: "I am giving two talks at ENUMATH 2019"
authors: []
tags: []
categories: []
date: 2019-09-26T09:15:10+01:00
lastmod: 2019-09-26T09:15:10+01:00
featured: false
draft: false
aliases:
  - /enumath2019

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---


I will be giving two talks at the [ENUMATH 2019](http://enumath2019.eu) conference in Egmond aan Zee, The Netherlands.

# A stable finite element method for an open, inextensible, viscoelastic rod with applications to nematode locomotion

- MS48: Mathematical and numerical solution of PDEs on manifolds
- Wednesday 2nd October, 9:20. Lecture room 533

## Abstract

We present and analyse a numerical method for understanding the dynamics of an open, inextensible viscoelastic rod - a long and thin three dimensional object. Our model allows for both elastic and viscous, bending and twisting deformations and describes the evolution of the midline curve of the rod as well as an orthonormal frame which full determines the rod's three dimensional geometry.
The numerical method is based on using a combination of piecewise linear and piecewise constant functions based on a novel rewriting of the model equations. We derive a stability estimate for the semi-discrete scheme and show that at the fully discrete level that we have good control over the length element and preserve the frame orthonormality conditions up to machine precision. Numerical experiments demonstrate both the good properties of the method as well as the applicability of the method for simulating locomotion of the microscopic nematode Caenorhabditis elegans.

## Slides

<div style="position:relative;padding-top:66%;">
  <iframe src="https://tom-ranner.gitlab.io/enumath-2019/viscoelastic-rod.embed.html" frameborder="0" allowfullscreen
    style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
</div>

<div class="btn-links mb-3">
<a class="btn btn-outline-primary my-1 mr-1" href="/publication/2019pp_viscoelaticrod/" target="_blank" rel="noopener">
  Preprint
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://arxiv.org/abs/1904.01325" target="_blank" rel="noopener">
  arXiv
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://tomranner.org/talk/enumath2019/viscoelastic-rod.html" target="_blank" rel="noopener">
  Slides
</a>
</div>

# A unified  theory for continuous in time evolving finite element space approximations to partial differential equations in evolving domains

- MS24: Computational surface PDEs
- Thursday 3rd October, 9:20. Lecture room 530

## Abstract ##

We develop a unified theory for continuous in time  finite element discretisations of partial differential equations posed in evolving domains including  the consideration of equations posed on evolving surfaces and bulk domains as well coupled surface bulk systems.
We use an abstract variational setting with time dependent function spaces and abstract time dependent finite element spaces.
Optimal a priori bounds are shown under usual assumptions on perturbations of bilinear forms and approximation properties of the abstract finite element spaces.
The abstract theory is applied to evolving finite elements in both flat and curved spaces.
The theory allows us to give precise definitions which relate the abstract theory to concrete constructions and show which assumptions lead to stability and error estimates.
Our approach allows an isoparametric approximation of parabolic equations in general domains.
Numerical experiments are described which confirm the rates of convergence.

## Slides

<div style="position:relative;padding-top:66%;">
  <iframe src="https://tomranner.org/talk/enumath2019/evolving-domains.embed.html" frameborder="0" allowfullscreen
    style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
</div>

<div class="btn-links mb-3">
<a class="btn btn-outline-primary my-1 mr-1" href="/publication/2019pp_evolvingdomains/" target="_blank" rel="noopener">
  Preprint
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://arxiv.org/abs/1703.04679" target="_blank" rel="noopener">
  arXiv
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://tom-ranner.gitlab.io/enumath-2019/evolving-domains.html" target="_blank" rel="noopener">
  Slides
</a>
</div>
