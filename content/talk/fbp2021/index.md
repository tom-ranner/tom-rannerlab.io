---
title: "Free Boundary Problems 2021"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2021-09-13T10:30:00+01:00
lastmod: 2021-09-13T10:30:00+01:00
featured: false
draft: false
aliases:
  - /fbp2021

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---

I will be giving a talk at the [Free Boundary Problems 2021](https://fbp2021.de) meeting as part of the Coupled Surface-Bulk problem mini-symposium.

# Numerical analysis of a coupled bulk-surface problem in an evolving domain

## Abstract

Based on an abstract theory for continuous in time finite element discretisations of partial differential equations posed in evolving domains, I will present numerical analysis of a simple coupled bulk-surface problem. The problem couples a linear parabolic equation in the evolving bulk domain to a linear parabolic equation posed on the boundary surface of the domain. Evolving bulk and surface isoparametric finite element spaces defined on evolving triangulations will be developed.Optimal a priori bounds are shown under usual assumptions on the geometry and solution of the partial differential equation.We conclude with a numerical example.

## Slides

<div style="position:relative;padding-top:66%;">
  <iframe src="https://tom-ranner.gitlab.io/fbp2021-talk/index.embed.html" frameborder="0" allowfullscreen
    style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
</div>

<div class="btn-links mb-3">
<a class="btn btn-outline-primary my-1 mr-1" href="/publication/2020_evolvingdomains/" target="_blank" rel="noopener">
  Paper
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://doi.org/10.1093/imanum/draa062" target="_blank" rel="noopener">
  doi
</a>
<a class="btn btn-outline-primary my-1 mr-1" href="https://tom-ranner.gitlab.io/fbp2021-talk/" target="_blank" rel="noopener">
  Slides
</a>
</div>

## Funding

This work was supported by a Leverhulme Trust early career fellowship.

