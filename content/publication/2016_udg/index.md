+++
authors = ["Christian Engwer", "ranner", "Sebastian Westerheide"]
date = "2016-01-01"
math = false
publication = "*Proceedings of ALGORITHMY 2016*"
title = "An unﬁtted discontinuous Galerkin scheme for conservation laws on evolving surfaces"
publication_types = ["1"]
featured = false
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1602.01080"
[[url_custom]]
    name = "link"
    url = "http://www.iam.fmph.uniba.sk/amuc/ojs/index.php/algoritmy/article/view/393/310"
+++
