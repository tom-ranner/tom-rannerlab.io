+++
authors = ["Charles M. Elliott", "ranner"]
date = "2015-01-11"
math = false
publication = "*Interfaces and Free Boundaries*, 17(3): 353-379"
title = "A computational approach to an optimal partition problem on surfaces"
publication_types = ["2"]
featured = false
doi = "10.4171/IFB/346"
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1408.2355"
+++
