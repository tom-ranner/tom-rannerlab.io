---
title: "A monolithic optimal control method for displacement tracking of Cosserat rod with application to reconstruction of C. elegans locomotion"
date: 2022-11-01
publishDate: 2022-11-15T09:25:59.167781Z
authors: ["Yongxing Wang", "ranner", "Thomas P. Ilett", "Yan Xia", "Netta Cohen"]
publication_types: ["2"]
abstract: ""
featured: false
publication: "*Computational Mechanics*"
url_pdf: "https://doi.org/10.1007/s00466-022-02247-x"
doi: "10.1007/s00466-022-02247-x"
---
