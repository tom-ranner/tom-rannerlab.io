+++
title = "Computation partial differential equations"
date = 2013-01-01
authors = ["ranner"]
publication_types = ["4"]
abstract = ""
featured = false
publication = "*PhD thesis*, University of Warwick"
url_pdf = "http://wrap.warwick.ac.uk/57647/"
+++
