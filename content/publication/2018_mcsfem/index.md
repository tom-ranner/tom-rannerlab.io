+++
authors = ["Ana Djurdjevac", "Charles M. Elliott", "Ralf Kornhuber", "ranner"]
featured = false
date = "2018-12-11"
math = false
publication = "*SIAM/ASA J. Uncertainty Quantification*, 6(4), 1656–1684."
title = "Evolving surface finite element methods for random advection-diffusion equations"
publication_types = ["2"]
abstract=" Even though random partial differential equations (PDEs) have become a very active field of research, the analysis and numerical analysis of random PDEs on surfaces still appears to be in its infancy. In this paper, we introduce and analyse a surface finite element discretization of  advection-diffusion equations with uncertain coefficients on evolving hypersurfaces. After stating unique solvability of the resulting semi-discrete problem, we prove optimal error bounds for the semi-discrete solution and Monte Carlo samplings of its expectation in appropriate Bochner spaces. Our theoretical findings are illustrated by numerical experiments  in two and three space dimensions."
abstract_short="Numerical analysis for Monte Carlo evolving surface finite element method."
url_pdf="pdf/DjuEllKor18.pdf"
url_code="https://github.com/tranner/dune-mcesfem"
doi = "10.1137/17M1149547"
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1702.07290"
+++

