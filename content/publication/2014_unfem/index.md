+++
authors = ["Klaus Deckelnick", "Charles M. Elliott", "ranner"]
date = "2014-08-19"
math = false
publication = "*SIAM Journal on Numerical Analysis*, 52 (4): 2136-2162"
title = "Unfitted finite element method using bulk meshes for surface partial differential equations"
doi = "10.1137/130948641"
publication_types = ["2"]
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1312.2905"
featured = false
+++

