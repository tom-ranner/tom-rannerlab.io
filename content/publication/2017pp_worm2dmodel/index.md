+++
authors = ["Netta Cohen", "ranner"]
date = "2017-02-01"
math = false
publication = "(preprint)"
title = "A new computational method for a model of Caenorhabditis elegans locomotion: Insights into elasticity and locomotion performance"
publication_types = ["3"]
featured = false
url_pdf = "pdf/CohRan17-pp.pdf"
abstract = " An organism's ability to move freely is a fundamental behaviour in the animal kingdom. To understand animal locomotion requires a characterisation of the material properties, as well as the biomechanics and physiology. We present a biomechanical model of C. elegans locomotion together with a novel finite element method. We formulate our model as a nonlinear initial-boundary value problem which allows the study of the dynamics of arbitrary body shapes, undulation gaits and the link between the animal's material properties and its performance across a range of environments. Our model replicates behaviours across a wide range of environments. It makes strong predictions on the viable range of the worm's Young's modulus and suggests that animals can control speed via the known mechanism of gait modulation that is observed across different media."
abstract_short="A new finite element method for C. elegans locomotion with insights into material parameters."
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1702.04988"
+++

