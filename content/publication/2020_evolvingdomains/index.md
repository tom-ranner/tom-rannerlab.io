+++
authors = ["Charles M. Elliott", "ranner"]
date = "2020-11-26"
math = false
publication = "IMA Journal of Numerical Analysis, Volume 41, Issue 3, July 2021, Pages 1696-1845, https://doi.org/10.1093/imanum/draa062"
title = "A unified  theory for continuous in time evolving finite element space approximations to partial differential equations in evolving domains"
publication_types = ["2"]
featured = false
abstract = "We develop a unified theory for continuous in time  finite element discretisations of partial differential equations posed in evolving domains including  the consideration of equations posed on evolving surfaces and bulk domains as well coupled surface bulk systems. We use  an abstract variational setting with time dependent function spaces and abstract time dependent finite element spaces. Optimal a priori bounds are shown under usual assumptions on perturbations of bilinear forms and approximation properties of the abstract finite element spaces. The abstract theory is applied to evolving  finite elements in both flat and curved spaces. Evolving bulk and surface isoparametric finite element spaces defined on evolving triangulations are defined and developed. These spaces are used to define approximations to  parabolic equations in general domains for which the abstract theory is shown to apply.   Numerical experiments are described which confirm the rates of convergence."
aliases = [
	"/publication/2019pp_evolvingdomains",
	"/publication/2021_evolvingdomains"
]
abstract_short="A complete numerical analysis for pde in evolving domains."
url_code="https://github.com/tranner/dune-evolving-domains"
doi="10.1093/imanum/draa062"
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1703.04679"
+++

**Funding** \
This work was support by a Leverhulme Early Career Fellowship.
