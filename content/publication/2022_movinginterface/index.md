+++
authors = ["Charles M. Elliott", "ranner", "Pierre Stepanov"]
date = "2024-01-24"
math = false
publication = "Submitted. Preprint: [arXiv:2208.04850](https://arxiv.org/abs/2208.04850)"
title = "Evolving finite elements for an advection diffusion problem with an  evolving interface"
publication_types = ["3"]
featured = false
abstract = " The aim of this paper is to develop a numerical scheme to approximate evolving interface problems for parabolic equations based on the abstract evolving finite element framework proposed in [(Elliott & Ranner 2021)](/publication/2020_evolvingdomains). An appropriate weak formulation of the problem is derived for the use of   evolving finite elements designed to accommodate for a moving  interface. Optimal order error bounds are proved for arbitrary order evolving  isoparametric finite elements. The paper concludes with numerical results for a model problem verifying orders of convergence. "
url_code="https://gitlab.com/tom-ranner/firedrake-moving-interfaces"
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/2208.04850"
+++

**Funding** \
This work was partially supported by a Leverhulme Early Career Fellowship.
