+++
authors = ["Jack E. Denham", "ranner", "Netta Cohen"]
date = "2018-09-10"
math = false
publication = "*Phil. Trans. R. Soc. B*, 373: 20180208."
title = "Signatures of proprioceptive control in C. elegans locomotion"
publication_types = ["2"]
featured = false
url_pdf = "pdf/DenRanCoh18.pdf"
abstract = "Animal neuromechanics describes the coordinated self-propelled movement of a body, subject to the combined effects of internal neural control and mechanical forces. Here we use a computational model to identify effects of neural and mechanical modulation on undulatory forward locomotion of C. elegans, with a focus on proprioceptively driven neural control. We reveal a fundamental relationship between body elasticity and environmental drag in determining the dynamics of the body and demonstrate the manifestation of this relationship in the context of proprioceptively driven control. By considering characteristics unique to proprioceptive neurons, we predict the signatures of internal gait modulation that contrast with the known signatures of externally or biomechanically modulated gait. We further show that proprioceptive feedback can suppress neuromechanical phase lags during undulatory locomotion, contrasting with well studied advancing phase lags that have long been a signature of centrally generated, feed-forward control."
doi = "10.1098/rstb.2018.0208"
url_code="https://bitbucket.org/leedswormlab/curve-worm-royal-society-paper"
[[url_custom]]
    name = "Suppl. Mat."
    url = "pdf/DenRanCoh18_supp.pdf"
+++

