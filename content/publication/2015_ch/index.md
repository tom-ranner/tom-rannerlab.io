+++
authors = ["Charles M. Elliott", "ranner"]
date = "2015-01-11"
publication = "*Numeriche Mathematik*, 129 (3): 483-534"
title = "Evolving surface finite element method for the Cahn-Hilliard equation"
doi = "10.1007/s00211-014-0644-y"
featured = false
publication_types = ["2"]
[[url_custom]]
    name = "arXiv"
    url = "https://arxiv.org/abs/1310.4068"
+++
