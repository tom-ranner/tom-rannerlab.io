+++
authors = ["Charles M. Elliott", "ranner"]
date = "2012-09-22"
math = false
publication = "*IMA Journal of Numerical Analysis*, 33 (2): 337-402"
title = "Finite element analysis for a coupled bulk-surface partial differential equation"
doi = "10.1093/imanum/drs022"
url_pdf = "pdf/EllRan13.pdf"
url_project = ""
url_slides = ""
url_video = ""
publication_types = ["2"]
featured = false
abstract = "In this paper, we define a new finite element method for numerically approximating the solution of a partial differential equation in a bulk region coupled with a surface partial differential equation posed on the boundary of the bulk domain. The key idea is to take a polyhedral approximation of the bulk region consisting of a union of simplices, and to use piecewise polynomial boundary faces as an approximation of the surface. Two finite element spaces are defined, one in the bulk region and one on the surface, by taking the set of all continuous functions which are also piecewise polynomial on each bulk simplex or boundary face. We study this method in the context of a model elliptic problem; in particular, we look at well-posedness of the system using a variational formulation, derive perturbation estimates arising from domain approximation and apply these to find the optimal-order error estimates. A numerical experiment is described which demonstrates the order of convergence."
+++


