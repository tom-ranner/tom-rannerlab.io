+++
authors = ["ranner"]
date = "2020-05-23"
math = false
publication = "Applied Numerical Mathematics 156 (2020) 422–44"
title = "A stable finite element method for low inertia undulatory locomotion in three dimensions"
publication_types = ["2"]
featured = false
abstract = "We present and analyse a numerical method for understanding the low-inertia dynamics of an open, inextensible viscoelastic rod - a long and thin three dimensional object - representing the body of a long, thin microswimmer. Our model allows for both elastic and viscous, bending and twisting deformations and describes the evolution of the midline curve of the rod as well as an orthonormal frame which fully determines the rod's three dimensional geometry. The numerical method is based on using a combination of piecewise linear and piecewise constant functions based on a novel rewriting of the model equations. We derive a stability estimate for the semi-discrete scheme and show that at the fully discrete level that we have good control over the length element and preserve the frame orthonormality conditions up to machine precision. Numerical experiments demonstrate both the good properties of the method as well as the applicability of the method for simulating undulatory locomotion in the low-inertia regime."
doi = "10.1016/j.apnum.2020.05.009"
aliases = [
	"/publication/2019pp_viscoelasticrod"
]
+++
**Funding** \
This work was support by a Leverhulme Early Career Fellowship.

## Videos:

- *relaxation*: This video shows the confirmation of the rod during the relaxation test.
  <div style="position:relative;padding-top: 56.25%">
    <iframe style="position:absolute;top:0;left:0;width:100%;height:100%" src="https://www.youtube-nocookie.com/embed/aih4-yPn0lk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>

- *locomotion 2d*: This video shows the confirmation of the body during the 2d locomotion test.
  <div style="position:relative;padding-top: 56.25%">
    <iframe style="position:absolute;top:0;left:0;width:100%;height:100%" src="https://www.youtube-nocookie.com/embed/mWtLNb93RGY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>

- *locomotion 3d*:  This video shows the confirmation of the body during the 3d locomotion test.
  <div style="position:relative;padding-top: 56.25%">
    <iframe style="position:absolute;top:0;left:0;width:100%;height:100%" src="https://www.youtube-nocookie.com/embed/FO8opOIykLs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
