+++
authors = ["Charles M. Elliott", "ranner", "Chandrashekar Venkataraman"]
title = "Coupled bulk-surface free boundary problems arising from a mathematical model of receptor-ligand dynamics"
publication = "*SIAM Journal on Mathematical Analysis*, 49 (1): 360-397"
date = "2017-02-08"
publication_types = ["2"]
abstract = "We consider a coupled bulk-surface system of partial differential equations with nonlinear coupling modelling receptor-ligand dynamics. The model arises as a simplification of a mathematical model for the reaction between cell surface resident receptors and ligands present in the extra-cellular medium. We prove the existence and uniqueness of solutions. We also consider a number of biologically relevant asymptotic limits of the model. We prove convergence to limiting problems which take the form of free boundary problems posed on the cell surface. We also report on numerical simulations illustrating convergence to one of the limiting problems as well as the spatio-temporal distributions of the receptors and ligands in a realistic geometry."
abstract_short = "Receptor-ligand dynamics, bulk-surface PDEs, free boundary problems, and bulk-surface finite elements"
featured = false
doi = "10.1137/15M1050811"
url_pdf = "pdf/EllRanVen17.pdf"
[[url_custom]]
name = "arXiv"
url = "https://arxiv.org/abs/1512.04889"
[[url_custom]]
name = "doi"
url = "http://dx.doi.org/10.1137/15M1050811"
+++

